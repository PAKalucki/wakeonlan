package kalucki.przemyslaw.wakeonlan.UI


import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.view.ViewGroup
import kalucki.przemyslaw.wakeonlan.Database.DeviceDatabase
import kalucki.przemyslaw.wakeonlan.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EditTest
{

	@Rule
	@JvmField
	var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

	@After
	fun clearDatabase()
	{
		DeviceDatabase.getDatabase(InstrumentationRegistry.getContext()).deviceDao().deleteAll()
	}

	@Test
	fun addValidDataAndEditDataShouldBeChanged()
	{
		val macToBeSet = "00:00:00:00:00:00"

		//enter data and press OK
		onView(allOf(withId(R.id.fab), isDisplayed())).perform(click())
		onView(allOf(withId(R.id.macEditText), isDisplayed())).perform(replaceText(macToBeSet), closeSoftKeyboard())
		onView(allOf(withId(R.id.buttonOk), isDisplayed())).perform(click())

		//go to edit
		onView(allOf(withId(R.id.itemTextView), withText(macToBeSet), isDisplayed())).perform(longClick())
		onView(allOf(withId(android.R.id.title), withText("Edit"), isDisplayed())).perform(click())

		//verify data
		onView(allOf<View>(withId(R.id.titleEditText), isDisplayed())).check(matches(withText(macToBeSet)))
		onView(allOf<View>(withId(R.id.ipEditText), isDisplayed())).check(matches(withText("192.168.1.255")))
		onView(allOf<View>(withId(R.id.macEditText), isDisplayed())).check(matches(withText(macToBeSet)))
		onView(allOf<View>(withId(R.id.portEditText), isDisplayed())).check(matches(withText("9")))

		//change data and save
		onView(allOf(withId(R.id.ipEditText), withText("192.168.1.255"), isDisplayed())).perform(replaceText("192.168.1.254"))
		onView(allOf(withId(R.id.buttonOk), withText("OK"), isEnabled())).perform(click())

		//go to edit
		onView(allOf(withId(R.id.itemTextView), withText(macToBeSet), isDisplayed())).perform(longClick())
		onView(allOf(withId(android.R.id.title), withText("Edit"), isDisplayed())).perform(click())

		//verify data was changed
		onView(allOf<View>(withId(R.id.titleEditText), isDisplayed())).check(matches(withText(macToBeSet)))
		onView(allOf<View>(withId(R.id.ipEditText), isDisplayed())).check(matches(withText("192.168.1.254")))
		onView(allOf<View>(withId(R.id.macEditText), isDisplayed())).check(matches(withText(macToBeSet)))
		onView(allOf<View>(withId(R.id.portEditText), isDisplayed())).check(matches(withText("9")))
	}

	@Test
	fun addValidDataAndEditWithInvalidDataOkButtonShouldBeDisabled()
	{
		val macToBeSet = "00:00:00:00:00:00"

		//enter data and press OK
		onView(allOf(withId(R.id.fab), isDisplayed())).perform(click())
		onView(allOf(withId(R.id.macEditText), isDisplayed())).perform(replaceText(macToBeSet), closeSoftKeyboard())
		onView(allOf(withId(R.id.buttonOk), isDisplayed())).perform(click())

		//go to edit
		onView(allOf(withId(R.id.itemTextView), withText(macToBeSet), isDisplayed())).perform(longClick())
		onView(allOf(withId(android.R.id.title), withText("Edit"), isDisplayed())).perform(click())

		//verify data
		onView(allOf<View>(withId(R.id.titleEditText), isDisplayed())).check(matches(withText(macToBeSet)))
		onView(allOf<View>(withId(R.id.ipEditText), isDisplayed())).check(matches(withText("192.168.1.255")))
		onView(allOf<View>(withId(R.id.macEditText), isDisplayed())).check(matches(withText(macToBeSet)))
		onView(allOf<View>(withId(R.id.portEditText), isDisplayed())).check(matches(withText("9")))

		//change data
		onView(allOf(withId(R.id.ipEditText), withText("192.168.1.255"), isDisplayed())).perform(replaceText("adaweads"))

		//verify button is disabled
		onView(allOf<View>(withId(R.id.buttonOk), withText("OK"), isDisplayed())).check(matches(Matchers.not(isEnabled())))
	}
}
