package kalucki.przemyslaw.wakeonlan.UI

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import junit.framework.Assert.*
import kalucki.przemyslaw.wakeonlan.Database.DeviceDAO
import kalucki.przemyslaw.wakeonlan.Database.DeviceDatabase
import kalucki.przemyslaw.wakeonlan.Database.Device
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DatabaseTest
{
	private lateinit var dao: DeviceDAO
	private lateinit var testDatabase: DeviceDatabase

	@Before
	fun createDB()
	{
		val context = InstrumentationRegistry.getTargetContext()
		testDatabase = Room.inMemoryDatabaseBuilder(context, DeviceDatabase::class.java).allowMainThreadQueries().build()
		dao = testDatabase.deviceDao()
	}

	@After
	fun closeDB()
	{
		testDatabase.close()
	}

	@Test
	fun getAllShouldReturnEmptyList()
	{
		val result = dao.getAll()
		assertNotNull(result)
		assertTrue(result.isEmpty())
	}

	@Test
	fun getAllShouldReturnListOf3()
	{
		insertToDatabase(3)

		val result = dao.getAll()
		assertNotNull(result)
		assertEquals(3, result.size)
	}

	@Test
	fun deleteAllShouldReturnEmptyList()
	{
		insertToDatabase(10)
		assertEquals(10, dao.getAll().size)

		dao.deleteAll()

		assertEquals(0, dao.getAll().size)
	}

	@Test
	fun getByIdShouldReturnNull()
	{
		assertNull(dao.getById(0L))
	}

	@Test
	fun getByIdShouldReturnNotNull()
	{
		insertToDatabase(1)

		val result = dao.getById(1L)

		assertNotNull(result)
		assertEquals(1L, result?.id)
	}

	private fun insertToDatabase(numberOfEntities: Int, name: String = "", mac: String = "")
	{
		for (i in 1..numberOfEntities) dao.insert(Device(id = i.toLong(), name = name, mac = mac))
	}
}