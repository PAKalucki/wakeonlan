package kalucki.przemyslaw.wakeonlan.UI

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import kalucki.przemyslaw.wakeonlan.Database.DeviceDatabase
import kalucki.przemyslaw.wakeonlan.R
import kalucki.przemyslaw.wakeonlan.UI.Utils.RecyclerViewItemCountAssertion
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class AddTest
{
	@Rule
	@JvmField
	val mActivityTestRule = ActivityTestRule(MainActivity::class.java)

	@After
	fun clearDatabase()
	{
		DeviceDatabase.getDatabase(InstrumentationRegistry.getContext()).deviceDao().deleteAll()
	}

	@Test
	fun addValidDataConfirmNewItemAdded()
	{
		onView(allOf<View>(withId(R.id.fab), isDisplayed())).perform(click())
		onView(allOf<View>(withId(R.id.titleEditText), isDisplayed())).perform(replaceText("test"), closeSoftKeyboard())
		onView(allOf<View>(withId(R.id.ipEditText), isDisplayed())).perform(replaceText("192.168.1.1"), closeSoftKeyboard())
		onView(allOf<View>(withId(R.id.macEditText), isDisplayed())).perform(replaceText("00:0A:E6:3E:FD:E1"), closeSoftKeyboard())
		onView(allOf<View>(withId(R.id.portEditText), isDisplayed())).perform(replaceText("9"), closeSoftKeyboard())

		onView(allOf<View>(withId(R.id.buttonOk), withText("OK"), isDisplayed(), isEnabled())).perform(click())

		onView(allOf(withId(R.id.recyclerView), isDisplayed())).check(RecyclerViewItemCountAssertion(1))
		onView(allOf<View>(withId(R.id.itemTextView), isDisplayed())).check(matches(withText("test")))
	}

	@Test
	fun insertInvalidDataCheckIfButtonOkIsDisabled()
	{
		onView(allOf<View>(withId(R.id.fab), isDisplayed())).perform(click())
		onView(allOf<View>(withId(R.id.titleEditText), isDisplayed())).perform(replaceText("test"), closeSoftKeyboard())
		onView(allOf<View>(withId(R.id.ipEditText), isDisplayed())).perform(replaceText("192.168.1.1"), closeSoftKeyboard())
		onView(allOf<View>(withId(R.id.macEditText), isDisplayed())).perform(replaceText("00:0A:E6:3E:FD:E1:"), closeSoftKeyboard())
		onView(allOf<View>(withId(R.id.portEditText), isDisplayed())).perform(replaceText("9"), closeSoftKeyboard())

		onView(allOf<View>(withId(R.id.buttonOk), withText("OK"), isDisplayed())).check(matches(not(isEnabled())))
	}

	@Test
	fun addMultipleDevicesThenDeleteExpectAllDeleted()
	{
		//add 3 new entries
		onView(allOf(withId(R.id.fab), isDisplayed())).perform(click())
		onView(allOf(withId(R.id.macEditText), isDisplayed())).perform(replaceText("00:00:00:00:00:01"), closeSoftKeyboard())
		onView(allOf(withId(R.id.buttonOk), withText("OK"), isDisplayed())).perform(click())
		onView(allOf(withId(R.id.fab), isDisplayed())).perform(click())
		onView(allOf(withId(R.id.macEditText), isDisplayed())).perform(replaceText("00:00:00:00:00:02"), closeSoftKeyboard())
		onView(allOf(withId(R.id.buttonOk), withText("OK"), isDisplayed())).perform(click())
		onView(allOf(withId(R.id.fab), isDisplayed())).perform(click())
		onView(allOf(withId(R.id.macEditText), isDisplayed())).perform(replaceText("00:00:00:00:00:03"), closeSoftKeyboard())
		onView(allOf(withId(R.id.buttonOk), withText("OK"), isDisplayed())).perform(click())

		//check if 3 added
		onView(allOf(withId(R.id.recyclerView), isDisplayed())).check(RecyclerViewItemCountAssertion(3))

		//delete 3
		onView(allOf(withId(R.id.itemTextView), withText("00:00:00:00:00:01"), isDisplayed())).perform(longClick())
		onView(allOf(withId(android.R.id.title), withText("Delete"), isDisplayed())).perform(click())
		onView(allOf(withId(R.id.itemTextView), withText("00:00:00:00:00:02"), isDisplayed())).perform(longClick())
		onView(allOf(withId(android.R.id.title), withText("Delete"), isDisplayed())).perform(click())
		onView(allOf(withId(R.id.itemTextView), withText("00:00:00:00:00:03"), isDisplayed())).perform(longClick())
		onView(allOf(withId(android.R.id.title), withText("Delete"), isDisplayed())).perform(click())

		//check if all removed
		onView(allOf(withId(R.id.recyclerView))).check(RecyclerViewItemCountAssertion(0))
	}
}