package kalucki.przemyslaw.wakeonlan.UI.Utils

import android.support.test.espresso.NoMatchingViewException
import android.support.test.espresso.ViewAssertion
import android.support.v7.widget.RecyclerView
import android.view.View
import junit.framework.Assert.assertTrue

/**
 * Created by przemek on 26.03.18.
 */
class RecyclerViewItemCountAssertion(private val expectedCount: Int) : ViewAssertion
{

	override fun check(view: View, noViewFoundException: NoMatchingViewException?)
	{
		if (noViewFoundException != null)
		{
			throw noViewFoundException
		}

		val recyclerView = view as RecyclerView
		val adapter = recyclerView.adapter
		assertTrue(adapter.itemCount == expectedCount)
	}
}