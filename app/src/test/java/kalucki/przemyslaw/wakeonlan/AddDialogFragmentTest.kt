package kalucki.przemyslaw.wakeonlan

import android.support.v4.app.DialogFragment
import kalucki.przemyslaw.wakeonlan.UI.AddDialogFragment
import kalucki.przemyslaw.wakeonlan.UI.MainActivity
import kotlinx.android.synthetic.main.content_base_dialog_fragment.*
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil.startFragment

@RunWith(RobolectricTestRunner::class)
class AddDialogFragmentTest
{
	lateinit var fragment: DialogFragment

	@Before
	fun setUp()
	{
		fragment = AddDialogFragment()
		startFragment(fragment, MainActivity::class.java)
	}

	@Test
	fun startFragmentExpectedElementsShouldExist()
	{
		assertNotNull(fragment.buttonOk)
		assertNotNull(fragment.buttonCancel)
		assertNotNull(fragment.ipEditText)
		assertNotNull(fragment.macEditText)
		assertNotNull(fragment.portEditText)
		assertNotNull(fragment.titleEditText)
	}

	@Test
	fun clickCancelExpectedFragmentDie()
	{
		assertTrue(fragment.isVisible)
		fragment.buttonCancel.performClick()
		assertTrue(!fragment.isVisible)
	}
}