package kalucki.przemyslaw.wakeonlan

import kalucki.przemyslaw.wakeonlan.Utils.validateIsIpAddress
import kalucki.przemyslaw.wakeonlan.Utils.validateIsMacAddress
import org.junit.Test
import org.junit.Assert.*

/**
 * Created by przemek on 25.03.18.
 */
class UtilsTest
{
  @Test
  fun testMacValidationValidData()
  {
    assertTrue("00:0A:E6:3E:FD:E1".validateIsMacAddress())
    assertTrue("00:0a:e6:3e:fd:e1".validateIsMacAddress())
    assertTrue("02:42:B9:72:89:5E".validateIsMacAddress())
    assertTrue("02:42:b9:72:89:5e".validateIsMacAddress())
  }

  @Test
  fun testMacValidationInvalidData()
  {
    assertFalse("00:0A:E6:3E:FD".validateIsMacAddress())
    assertFalse("000a:e6:3e:fd:e1".validateIsMacAddress())
    assertFalse("XX:42:B9:72:89:5E".validateIsMacAddress())
    assertFalse("025:425:b95:725:895:5e5".validateIsMacAddress())
  }

  @Test
  fun testIpValidationValidData()
  {
    assertTrue("192.168.1.1".validateIsIpAddress())
    assertTrue("255.255.255.255".validateIsIpAddress())
    assertTrue("127.0.0.1".validateIsIpAddress())
    assertTrue("193.109.239.176".validateIsIpAddress())
  }

  @Test
  fun testIpValidationInvalidData()
  {
    assertFalse("192.168.1.256".validateIsIpAddress())
    assertFalse("256.255.255.255".validateIsIpAddress())
    assertFalse("127.0.0.1.1".validateIsIpAddress())
    assertFalse("193.109".validateIsIpAddress())
    assertFalse("1.1.1".validateIsIpAddress())
  }
}