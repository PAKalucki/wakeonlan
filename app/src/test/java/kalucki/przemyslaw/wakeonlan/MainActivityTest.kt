package kalucki.przemyslaw.wakeonlan

import kalucki.przemyslaw.wakeonlan.UI.AddDialogFragment
import kalucki.przemyslaw.wakeonlan.UI.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class MainActivityTest
{
	lateinit var activity: MainActivity

	@Before
	fun setUp()
	{
		activity = Robolectric.setupActivity(MainActivity::class.java)
	}

	@Test
	fun startActivityExpectedElementsShouldExist()
	{
		assertNotNull(activity.fab)
		assertNotNull(activity.recyclerView)
		assertNotNull(activity.toolbar)
	}

	@Test
	fun clickAddShouldStartAddDialogFragment()
	{
		activity.fab.performClick()

		val dialogFragment = activity.supportFragmentManager.findFragmentByTag("ADD") as AddDialogFragment
		assertNotNull(dialogFragment)
	}
}