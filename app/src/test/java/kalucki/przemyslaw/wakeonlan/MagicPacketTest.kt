package kalucki.przemyslaw.wakeonlan

import kalucki.przemyslaw.wakeonlan.Database.Device
import kalucki.przemyslaw.wakeonlan.WOL.MagicPacket
import org.junit.Test

/**
 * Created by przemek on 25.03.18.
 */
class MagicPacketTest
{
  @Test
  fun testConstructorValidData()
  {
    val validData = Device(0, "test", "192.168.1.1", "00:0A:E6:3E:FD:E1", 20)
	  MagicPacket(validData)
  }

  @Test(expected = IllegalArgumentException::class)
  fun testConstructorInvalidMacAddress()
  {
    val invalidData = Device(0, "", "192.168.1.1", "1234asb", 0)
	  MagicPacket(invalidData)
  }

  @Test(expected = IllegalArgumentException::class)
  fun testConstructorInvalidIpAddress()
  {
    val invalidData = Device(0, "", "192.168.1.1.1", "00:0A:E6:3E:FD:E1", 0)
	  MagicPacket(invalidData)
  }
}