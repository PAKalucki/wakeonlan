package kalucki.przemyslaw.wakeonlan.Widget

import android.content.Context
import android.content.SharedPreferences
import kalucki.przemyslaw.wakeonlan.Utils.editAndApply
import kalucki.przemyslaw.wakeonlan.Utils.editAndCommit

class WidgetPreferences(context: Context)
{
	private val preferences: SharedPreferences = context.getSharedPreferences("kalucki.przemyslaw.wakeonlan.pref", Context.MODE_PRIVATE)

	fun add(widgetId: Int, name: String, id: Long)
	{
		preferences.editAndCommit {
			putString("Name$widgetId", name)
			putLong("Id$widgetId", id)
		}
	}

	fun getName(widgetId: Int): String = preferences.getString("Name$widgetId", "")

	fun getId(widgetId: Int): Long = preferences.getLong("Id$widgetId", -1L)

	fun delete(widgetId: Int)
	{
		preferences.editAndApply {
			remove("Name$widgetId")
			remove("Id$widgetId")
		}
	}
}