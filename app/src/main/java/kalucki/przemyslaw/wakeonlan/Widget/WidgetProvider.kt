package kalucki.przemyslaw.wakeonlan.Widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.RemoteViews
import kalucki.przemyslaw.wakeonlan.Database.DeviceDAO
import kalucki.przemyslaw.wakeonlan.Database.DeviceDatabase
import kalucki.przemyslaw.wakeonlan.R
import kalucki.przemyslaw.wakeonlan.Utils.runOnBackgroundThenPerformAction
import kalucki.przemyslaw.wakeonlan.Utils.showAsToastMessage
import kalucki.przemyslaw.wakeonlan.WOL.MagicPacket
import kalucki.przemyslaw.wakeonlan.WOL.MagicPacket.Companion.sendMagicPacket

class WidgetProvider : AppWidgetProvider()
{
	companion object
	{
		const val WIDGET_CLICKED = "kalucki.przemyslaw.wakeonlan.Widget.WIDGET_CLICKED"
		const val ID: String = "ID"

		fun updateWidgets(context: Context)
		{
			val intent = Intent(context, WidgetProvider::class.java)
			intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE

			val widgetIDs = AppWidgetManager.getInstance(context).getAppWidgetIds(ComponentName(context, WidgetProvider::class.java))
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIDs)
			context.sendBroadcast(intent)
		}

		private fun getPendingIntentOnClick(context: Context, action: String, entityId: Long): PendingIntent
		{
			val intent = Intent(context, WidgetProvider::class.java)

			intent.action = action + entityId //this needs to be unique to avoid widgets overriding
			val bundle = Bundle()
			bundle.putLong(ID, entityId)
			intent.putExtras(bundle)
			return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
		}

		fun getRemoteViews(context: Context, widgetName: String, entityId: Long): RemoteViews
		{
			val remoteViews = RemoteViews(context.packageName, R.layout.widget)
			remoteViews.setTextViewText(R.id.title, widgetName)
			remoteViews.setOnClickPendingIntent(R.id.title, getPendingIntentOnClick(context, WIDGET_CLICKED, entityId))

			return remoteViews
		}

		fun updateWidgetUI(context: Context, appWidgetManager: AppWidgetManager, widgetPreferences: WidgetPreferences, widgetId: Int)
		{
			val widgetName = widgetPreferences.getName(widgetId)
			val entityId = widgetPreferences.getId(widgetId)

			appWidgetManager.updateAppWidget(widgetId, getRemoteViews(context, widgetName, entityId))
		}
	}

	override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray)
	{
		super.onUpdate(context, appWidgetManager, appWidgetIds)
		val widgetPreferences = WidgetPreferences(context)

		appWidgetIds.forEach { updateWidgetUI(context, appWidgetManager, widgetPreferences, it) }
	}

	override fun onDeleted(context: Context, appWidgetIds: IntArray)
	{
		super.onDeleted(context, appWidgetIds)
		val widgetPreferences = WidgetPreferences(context)

		appWidgetIds.forEach { widgetPreferences.delete(it) }
	}

	override fun onReceive(context: Context, intent: Intent)
	{
		super.onReceive(context, intent)

		if (intent.action.startsWith(WIDGET_CLICKED))
		{
			val id = intent.getLongExtra(ID, -1L)

			if (id != -1L)
			{
				val db: DeviceDAO = DeviceDatabase.getDatabase(context).deviceDao()
				runOnBackgroundThenPerformAction(action = {
					                                 val entity = db.getById(id)
					                                 if (entity != null)
					                                 {
						                                 sendMagicPacket(MagicPacket(entity))
					                                 }
					                                 else false
				                                 },
				                                 onSuccess =  { "Packet sent".showAsToastMessage(context) },
				                                 onFailure = { "Outdated widget, please remove it".showAsToastMessage(context) })
			}
		}
	}
}