package kalucki.przemyslaw.wakeonlan.Widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle

class WidgetPinnedReceiver : BroadcastReceiver()
{
	companion object
	{
		const val WIDGET_NAME = "WIDGET_NAME"
		const val ID: String = "ID"

		fun getPendingIntent(context: Context, widgetName: String, entityId: Long): PendingIntent
		{
			val callbackIntent = Intent(context, WidgetPinnedReceiver::class.java)
			val bundle = Bundle()
			bundle.putString(WIDGET_NAME, widgetName)
			bundle.putLong(ID, entityId)
			callbackIntent.putExtras(bundle)
			return PendingIntent.getBroadcast(context, 0, callbackIntent, PendingIntent.FLAG_UPDATE_CURRENT)
		}
	}

	override fun onReceive(context: Context?, intent: Intent?)
	{
		context?.let { c ->
			intent?.let { i ->
				val widgetId = i.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1)

				if (widgetId != -1)
				{
					val name = i.getStringExtra(WIDGET_NAME)
					val id = i.getLongExtra(ID, -1L)
					val widgetPreferences = WidgetPreferences(c)

					widgetPreferences.add(widgetId, name, id)
					WidgetProvider.updateWidgets(c)
				}
			}
		}
	}
}