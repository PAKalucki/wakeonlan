package kalucki.przemyslaw.wakeonlan.WOL

import kalucki.przemyslaw.wakeonlan.Database.Device
import kalucki.przemyslaw.wakeonlan.Utils.validateIsIpAddress
import kalucki.przemyslaw.wakeonlan.Utils.validateIsMacAddress
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

class MagicPacket(val entity: Device)
{
	var bytes = ByteArray(6)

	init
	{
		//TODO refactor magic
		if (entity.mac.validateIsMacAddress() && entity.ip.validateIsIpAddress())
		{
			val macBytes = entity.mac.split(':').map({ value -> (Integer.parseInt(value, 16)).toByte() })

			bytes.fill(0xff.toByte(), 0, 6)

			for (i in 6..96 step macBytes.size)
			{
				bytes = bytes.plus(macBytes)
			}
		}
		else
		{
			throw IllegalArgumentException("Invalid mac or ip address")
		}
	}

	companion object
	{
		fun sendMagicPacket(packet: MagicPacket): Boolean
		{
			return try
			{
				val address = InetAddress.getByName(packet.entity.ip)
				val datagramPacket = DatagramPacket(packet.bytes, packet.bytes.size, address, packet.entity.port)
				val socket = DatagramSocket()

				socket.send(datagramPacket)
				socket.close()

				true
			}
			catch (e: Exception)
			{
				false
			}
		}
	}
}