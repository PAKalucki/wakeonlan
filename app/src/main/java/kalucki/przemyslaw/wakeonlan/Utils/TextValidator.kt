package kalucki.przemyslaw.wakeonlan.Utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView

abstract class TextValidator(private val textView: TextView) : TextWatcher
{
  override fun afterTextChanged(p0: Editable?)
  {
    validate(textView.text.toString())
  }

  override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int)
  {
  }

  override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int)
  {
  }

  abstract fun validate(text: String)
}