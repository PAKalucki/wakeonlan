package kalucki.przemyslaw.wakeonlan.Utils

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import kalucki.przemyslaw.wakeonlan.BuildConfig
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import kotlin.coroutines.experimental.CoroutineContext

inline fun String.validateIsMacAddress() = matches(Regex("((([0-9a-fA-F]){2}[-:]){5}([0-9a-fA-F]){2})"))

inline fun String.validateIsIpAddress() = matches(
		Regex("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"))

inline fun Any.showAsToastMessage(context: Context)
{
	Toast.makeText(context, this.toString(), Toast.LENGTH_SHORT).show()
}

inline fun runOnBackground(crossinline func1: () -> Unit, coroutineContext: CoroutineContext = UI)
{
	async(coroutineContext) {
		bg { func1() }.await() //TODO 1. do i rly need await() 2. Check exception handling with coroutines!!!
	}
}

inline fun runOnBackgroundThenPerformAction(crossinline action: () -> Boolean, crossinline onSuccess: () -> Unit = {}, crossinline onFailure: () -> Unit = {}, coroutineContext: CoroutineContext = UI)
{
	async(coroutineContext) {
		val result = bg {
			action()
		}.await()

		if (result) onSuccess() else onFailure()
	}
}

inline fun SharedPreferences.editAndApply(func: SharedPreferences.Editor.() -> Unit)
{
	val editor = edit()
	editor.func()
	editor.apply()
}

inline fun SharedPreferences.editAndCommit(func: SharedPreferences.Editor.() -> Unit)
{
	val editor = edit()
	editor.func()
	editor.commit()
}

inline fun LOG_DEBUG(tag: String, message: String)
{
	if (BuildConfig.DEBUG) Log.i(tag, message)
}