package kalucki.przemyslaw.wakeonlan.Recycler

import android.support.v7.widget.RecyclerView
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import kalucki.przemyslaw.wakeonlan.R
import kalucki.przemyslaw.wakeonlan.UI.EventListener

private const val WAKE = 0
private const val EDIT = 1
private const val DELETE = 2
private const val CREATE_WIDGET = 3

class BasicViewHolder(itemView: View, listener: EventListener) : RecyclerView.ViewHolder(itemView), View.OnCreateContextMenuListener
{
	val text: TextView = itemView.findViewById(R.id.itemTextView)
	val foreground: LinearLayout = itemView.findViewById(R.id.view_foreground)

	init
	{
		foreground.setOnCreateContextMenuListener(this)
	}

	override fun onCreateContextMenu(menu: ContextMenu?, view: View?, menuInfo: ContextMenu.ContextMenuInfo?)
	{
		menu?.let { setupMenu(it, text.text.toString(), arrayOf("Wake", "Edit", "Delete", "Create Widget")) }
	}

	private fun setupMenu(menu: ContextMenu, headerTitle: String, items: Array<String>)
	{
		menu.setHeaderTitle(headerTitle)

		items.forEachIndexed { index, s ->
			val menuItem = menu.add(Menu.NONE, index, index, s)
			menuItem.setOnMenuItemClickListener(onEditMenu)
		}
	}

	private val onEditMenu = MenuItem.OnMenuItemClickListener {
		when (it.itemId)
		{
			WAKE ->
			{
				listener.onWakeClicked(adapterPosition)
				true
			}
			EDIT ->
			{
				listener.onUpdateClicked(adapterPosition)
				true
			}
			DELETE ->
			{
				listener.onDeleteClicked(adapterPosition)
				true
			}
			CREATE_WIDGET ->
			{
				listener.onCreateWidgetClicked(adapterPosition)
				true
			}
			else -> false
		}
	}

}