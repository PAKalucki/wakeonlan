package kalucki.przemyslaw.wakeonlan.Recycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import kalucki.przemyslaw.wakeonlan.Database.Device
import kalucki.przemyslaw.wakeonlan.R
import kalucki.przemyslaw.wakeonlan.UI.EventListener

class BasicAdapter(private val listItems: MutableList<Device>, context: Context) : RecyclerView.Adapter<BasicViewHolder>()
{
	private val listener: EventListener = context as EventListener

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = BasicViewHolder(
			LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false), listener)

	override fun onBindViewHolder(holder: BasicViewHolder, position: Int)
	{
		holder.apply {
			text.text = listItems[position].name
		}
	}

	override fun getItemCount() = listItems.size

	fun removeItem(position: Int)
	{
		listItems.removeAt(position)
		notifyItemRemoved(position)
	}

	fun restoreItem(entity: Device, position: Int)
	{
		listItems.add(position, entity)
		notifyItemInserted(position)
	}

	fun addItem(entity: Device)
	{
		listItems.add(entity)
		notifyDataSetChanged()
	}

	fun updateItem(entity: Device, position: Int)
	{
		listItems[position] = entity
		notifyItemChanged(position)
	}
}