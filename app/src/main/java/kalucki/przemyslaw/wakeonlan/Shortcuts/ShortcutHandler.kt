package kalucki.przemyslaw.wakeonlan.Shortcuts

import android.content.Context
import android.content.Intent
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.drawable.Icon
import kalucki.przemyslaw.wakeonlan.Database.DeviceDatabase
import kalucki.przemyslaw.wakeonlan.Database.Device
import kalucki.przemyslaw.wakeonlan.R
import kalucki.przemyslaw.wakeonlan.Utils.runOnBackground

private const val SHORTCUT_CLICKED = "kalucki.przemyslaw.wakeonlan.Shortcuts.SHORTCUTCLICKED"
private const val ID: String = "ID"

class ShortcutHandler(private val context: Context, private val shortcutManager: ShortcutManager) //TODO handling outdated pinned shortcuts
{

	fun loadShortcuts()
	{
		runOnBackground({ getDatabaseInfoAndCreateUpTo4Shortcuts() })
	}

	private fun getDatabaseInfoAndCreateUpTo4Shortcuts()
	{
		shortcutManager.dynamicShortcuts = DeviceDatabase.getDatabase(context).deviceDao().getAll().take(4).map { entity -> createShortcutFromEntity(entity) }
	}

	private fun createShortcutFromEntity(entity: Device): ShortcutInfo
	{
		val intent = Intent(SHORTCUT_CLICKED)
		intent.setClass(context, ShortcutActivity::class.java)
		intent.putExtra(ID, entity.id)

		val icon = Icon.createWithResource(context, R.drawable.ic_power_settings_grey)

		return createShortcutInfo(id = entity.id.toString(), shortLabel = entity.name, icon = icon, intent = intent)
	}

	private fun createShortcutInfo(id: String, shortLabel: String, longLabel: String = shortLabel, icon: Icon? = null, intent: Intent): ShortcutInfo
	{
		return ShortcutInfo.Builder(context, id).setShortLabel(shortLabel).setLongLabel(longLabel).setIcon(icon).setIntent(intent).build()
	}
}