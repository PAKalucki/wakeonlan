package kalucki.przemyslaw.wakeonlan.Shortcuts

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kalucki.przemyslaw.wakeonlan.Database.DeviceDAO
import kalucki.przemyslaw.wakeonlan.Database.DeviceDatabase
import kalucki.przemyslaw.wakeonlan.Utils.LOG_DEBUG
import kalucki.przemyslaw.wakeonlan.Utils.runOnBackgroundThenPerformAction
import kalucki.przemyslaw.wakeonlan.Utils.showAsToastMessage
import kalucki.przemyslaw.wakeonlan.WOL.MagicPacket

private const val SHORTCUT_CLICKED = "kalucki.przemyslaw.wakeonlan.Shortcuts.SHORTCUTCLICKED"

class ShortcutActivity : AppCompatActivity()
{

	override fun onCreate(savedInstanceState: Bundle?)
	{
		super.onCreate(savedInstanceState)

		if (intent.action.startsWith(SHORTCUT_CLICKED))
		{
			LOG_DEBUG("SHORTCUT", " Clicked")
			val id = intent.getLongExtra("ID", -1L)

			if (id != -1L)
			{
				LOG_DEBUG("SHORTCUT", " Proper ID")
				val db: DeviceDAO = DeviceDatabase.getDatabase(applicationContext).deviceDao()

				runOnBackgroundThenPerformAction(action = {
					val entity = db.getById(id)
					LOG_DEBUG("SHORTCUT", " Got Entity")
					if (entity != null)
					{
						LOG_DEBUG("SHORTCUT", "Entity not null $entity")
						MagicPacket.sendMagicPacket(MagicPacket(entity))
					}
					else false

				}, onSuccess = { "Packet sent".showAsToastMessage(applicationContext) },
           		onFailure = { "Something went wrong".showAsToastMessage(applicationContext) })
			}
		}

		finish()
	}
}
