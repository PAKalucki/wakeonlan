package kalucki.przemyslaw.wakeonlan.UI

import android.content.Context
import android.os.Bundle
import android.view.View
import kalucki.przemyslaw.wakeonlan.Database.Device
import kotlinx.android.synthetic.main.content_base_dialog_fragment.*

class EditDialogFragment : BaseDialogFragment()
{
	private lateinit var listener: EventListener
	private lateinit var entity: Device
	private val position by lazy { tag!!.toInt() }

	override fun onAttach(context: Context?)
	{
		listener = context as EventListener
		entity = listener.onRequestData(position)
		super.onAttach(context)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?)
	{
		super.onViewCreated(view, savedInstanceState)

		setDataToView(entity.name, entity.ip, entity.mac, entity.port.toString())
	}

	override fun doOnOk()
	{
		listener.onUpdate(setDataToEntity(entity), position)
		dismiss()
	}

	private fun setDataToView(title: String, ip: String, mac: String, port: String)
	{
		titleEditText.setText(title)
		ipEditText.setText(ip)
		macEditText.setText(mac)
		portEditText.setText(port)
	}

	//TODO code duplication
	private fun setDataToEntity(entity: Device): Device
	{
		entity.apply {
			name = if (titleEditText.text.isNullOrEmpty()) macEditText.text.toString() else titleEditText.text.toString()
			ip = if (ipEditText.text.isNullOrEmpty()) "192.168.1.255" else ipEditText.text.toString()
			mac = macEditText.text.toString()
			port = portEditText.text.toString().toIntOrNull() ?: 9
		}
		return entity
	}
}