package kalucki.przemyslaw.wakeonlan.UI

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kalucki.przemyslaw.wakeonlan.R

class AboutDialogFragment : DialogFragment()
{
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, bundle: Bundle?): View
	{
		return inflater.inflate(R.layout.content_about_dialog_fragment, container, false)
	}
}