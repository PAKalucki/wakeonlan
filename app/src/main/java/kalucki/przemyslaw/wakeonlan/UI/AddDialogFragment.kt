package kalucki.przemyslaw.wakeonlan.UI

import android.content.Context
import kalucki.przemyslaw.wakeonlan.Database.Device
import kotlinx.android.synthetic.main.content_base_dialog_fragment.*

class AddDialogFragment : BaseDialogFragment()
{
	private lateinit var listener: EventListener

	override fun onAttach(context: Context?)
	{
		listener = context as EventListener

		super.onAttach(context)
	}

	override fun doOnOk()
	{
		listener.onInsert(getEnteredData())
		dismiss()
	}

	//TODO code duplication
	private fun getEnteredData(): Device
	{
		return Device(name = if (titleEditText.text.isNullOrEmpty()) macEditText.text.toString() else titleEditText.text.toString(),
		              ip = if (ipEditText.text.isNullOrEmpty()) "192.168.1.255" else ipEditText.text.toString(),
		              mac = macEditText.text.toString(), port = portEditText.text.toString().toIntOrNull() ?: 9)
	}
}