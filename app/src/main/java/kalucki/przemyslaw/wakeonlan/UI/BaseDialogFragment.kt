package kalucki.przemyslaw.wakeonlan.UI

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kalucki.przemyslaw.wakeonlan.R
import kalucki.przemyslaw.wakeonlan.Utils.TextValidator
import kalucki.przemyslaw.wakeonlan.Utils.validateIsIpAddress
import kalucki.przemyslaw.wakeonlan.Utils.validateIsMacAddress
import kotlinx.android.synthetic.main.content_base_dialog_fragment.*

abstract class BaseDialogFragment : DialogFragment(), View.OnClickListener
{
	private var isIpValid = true
	private var isMacValid = false

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, bundle: Bundle?): View
	{
		return inflater.inflate(R.layout.content_base_dialog_fragment, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?)
	{
		super.onViewCreated(view, savedInstanceState)

		buttonOk.setOnClickListener(this)
		buttonCancel.setOnClickListener(this)

		//TODO ugly
		ipEditText.addTextChangedListener(object : TextValidator(ipEditText)
		                                  {
			                                  override fun validate(text: String)
			                                  {
				                                  if (text.validateIsIpAddress() || text.isEmpty()) //empty is fine, default value will be used
				                                  {
					                                  ipEditText.error = null
					                                  isIpValid = true
					                                  buttonOk.isEnabled = isIpValid && isMacValid
				                                  }
				                                  else
				                                  {
					                                  ipEditText.error = "Invalid Ip Address"
					                                  isIpValid = false
					                                  buttonOk.isEnabled = isIpValid && isMacValid
				                                  }
			                                  }
		                                  })

		macEditText.addTextChangedListener(object : TextValidator(macEditText)
		                                   {
			                                   override fun validate(text: String)
			                                   {
				                                   if (text.validateIsMacAddress())
				                                   {
					                                   macEditText.error = null
					                                   isMacValid = true
					                                   buttonOk.isEnabled = isIpValid && isMacValid
				                                   }
				                                   else
				                                   {
					                                   macEditText.error = "Invalid Mac Address"
					                                   isMacValid = false
					                                   buttonOk.isEnabled = isIpValid && isMacValid
				                                   }
			                                   }
		                                   })
	}

	override fun onClick(view: View?) = when (view?.id)
	{
		R.id.buttonOk -> doOnOk()
		R.id.buttonCancel -> doOnCancel()
		else ->
		{
		}
	}

	abstract fun doOnOk()

	private fun doOnCancel() = dismiss()
}