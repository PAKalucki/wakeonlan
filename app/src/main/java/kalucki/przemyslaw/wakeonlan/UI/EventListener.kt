package kalucki.przemyslaw.wakeonlan.UI

import kalucki.przemyslaw.wakeonlan.Database.Device

interface EventListener
{
	fun onInsert(entity: Device)
	fun onUpdate(entity: Device, position: Int)
	fun onWakeClicked(position: Int)
	fun onUpdateClicked(position: Int)
	fun onDeleteClicked(position: Int)
	fun onCreateWidgetClicked(position: Int)
	fun onRequestData(position: Int): Device
}