package kalucki.przemyslaw.wakeonlan.UI

import android.annotation.TargetApi
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import kalucki.przemyslaw.wakeonlan.Database.DeviceDatabase
import kalucki.przemyslaw.wakeonlan.Database.Device
import kalucki.przemyslaw.wakeonlan.R
import kalucki.przemyslaw.wakeonlan.Recycler.BasicAdapter
import kalucki.przemyslaw.wakeonlan.Utils.runOnBackgroundThenPerformAction
import kalucki.przemyslaw.wakeonlan.Utils.showAsToastMessage
import kalucki.przemyslaw.wakeonlan.WOL.MagicPacket
import kalucki.przemyslaw.wakeonlan.WOL.MagicPacket.Companion.sendMagicPacket
import kalucki.przemyslaw.wakeonlan.Widget.WidgetPinnedReceiver
import kalucki.przemyslaw.wakeonlan.Widget.WidgetProvider
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import android.content.pm.ShortcutManager
import kalucki.przemyslaw.wakeonlan.Shortcuts.ShortcutHandler
import kalucki.przemyslaw.wakeonlan.Utils.LOG_DEBUG
import kalucki.przemyslaw.wakeonlan.Utils.runOnBackground


class MainActivity : AppCompatActivity(), EventListener
{
	private val db by lazy { DeviceDatabase.getDatabase(applicationContext).deviceDao() }
	private val listItems = mutableListOf<Device>()
	private lateinit var basicAdapter: BasicAdapter

	override fun onCreate(savedInstanceState: Bundle?)
	{
		super.onCreate(savedInstanceState)

		setContentView(R.layout.activity_main)

		toolbar.title = ""
		setSupportActionBar(toolbar)
		fab.setOnClickListener { showAddDialog(supportFragmentManager, "ADD") }
		setupRecyclerView()
		loadDeviceList()
		loadShortcuts()
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean
	{
		menuInflater.inflate(R.menu.menu_main, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId)
	{
		R.id.action_about ->
		{
			showAboutDialog(supportFragmentManager, "ABOUT")
			true
		}
		else -> super.onOptionsItemSelected(item)
	}

	override fun onInsert(entity: Device)
	{
		LOG_DEBUG(localClassName, "onInsert")

		basicAdapter.addItem(entity)
		runOnBackground({ db.insert(entity) })
	}

	override fun onWakeClicked(position: Int)
	{
		LOG_DEBUG(localClassName, "onWakeClicked")

		val deviceToWakeUp = listItems[position]

		runOnBackgroundThenPerformAction(action = { sendMagicPacket(MagicPacket(deviceToWakeUp)) },
		                                 onSuccess = { "Packet sent to ${deviceToWakeUp.mac}".showAsToastMessage(applicationContext) },
		                                 onFailure = { "Something went wrong".showAsToastMessage(applicationContext) })
	}

	override fun onDeleteClicked(position: Int)
	{
		LOG_DEBUG(localClassName, "onDeleteClicked")

		deleteDevice(position)
	}

	override fun onCreateWidgetClicked(position: Int)
	{
		LOG_DEBUG(localClassName, "onCreateWidgetClicked")

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
		{
			requestPinWidget(position)
		}
		else "Sorry this feature requires at least Android Oreo".showAsToastMessage(applicationContext)
	}

	override fun onUpdate(entity: Device, position: Int)
	{
		LOG_DEBUG(localClassName, "onUpdate")

		basicAdapter.updateItem(entity, position)
		runOnBackground({ db.update(entity) })
	}

	override fun onUpdateClicked(position: Int)
	{
		LOG_DEBUG(localClassName, "onUpdateClicked")

		showEditDialog(supportFragmentManager, position.toString())
	}

	override fun onRequestData(position: Int) = listItems[position]

	private fun showAddDialog(manager: FragmentManager, tag: String) = AddDialogFragment().show(manager, tag)

	private fun showEditDialog(manager: FragmentManager, tag: String) = EditDialogFragment().show(manager, tag)

	private fun showAboutDialog(manager: FragmentManager, tag: String) = AboutDialogFragment().show(manager, tag)

	private fun updateList(entity: Device)
	{
		listItems.add(entity)
		basicAdapter.notifyDataSetChanged()
	}

	private fun loadDeviceList()
	{
		var devices: List<Device> = emptyList()

		runOnBackgroundThenPerformAction(action = {
			devices = db.getAll()
			devices.isNotEmpty()
		}, onSuccess = {
			listItems.clear()
			devices.forEach { device -> updateList(device) }
		})
	}

	private fun setupRecyclerView()
	{
		basicAdapter = BasicAdapter(listItems, this)

		recyclerView.apply {
			layoutManager = LinearLayoutManager(applicationContext)
			itemAnimator = DefaultItemAnimator()
			addItemDecoration(DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL))
			adapter = basicAdapter
		}
	}

	private fun deleteDevice(deletedIndex: Int)
	{
		val deletedItem = listItems[deletedIndex]

		basicAdapter.removeItem(deletedIndex)

		runOnBackground({ db.delete(deletedItem) })

		val snackBar: Snackbar = Snackbar.make(coordinator_layout, deletedItem.name + " removed", Snackbar.LENGTH_LONG)

		snackBar.setAction("UNDO") {
			basicAdapter.restoreItem(deletedItem, deletedIndex)
			runOnBackground({ db.insert(deletedItem) })
		}

		snackBar.setActionTextColor(Color.YELLOW)
		snackBar.show()
	}

	// TODO manual pinning works on emulator but no real device
	@TargetApi(Build.VERSION_CODES.O)
	private fun requestPinWidget(position: Int)
	{
		val widgetName = listItems[position].name
		val entityId = listItems[position].id
		val appWidgetManager = getSystemService(AppWidgetManager::class.java)
		val provider = ComponentName(applicationContext, WidgetProvider::class.java)

		if (!appWidgetManager.isRequestPinAppWidgetSupported)
		{
			"Sorry, widget pinning is not supported".showAsToastMessage(applicationContext)
			return
		}

		val successCallback = WidgetPinnedReceiver.getPendingIntent(this, widgetName, entityId)

		val remoteViews = WidgetProvider.getRemoteViews(this, widgetName, entityId)
		val bundle = Bundle()
		bundle.putParcelable(AppWidgetManager.EXTRA_APPWIDGET_PREVIEW, remoteViews)

		appWidgetManager.requestPinAppWidget(provider, bundle, successCallback)
	}

	private fun loadShortcuts()
	{
		val shortcutHandler = ShortcutHandler(applicationContext, getSystemService(ShortcutManager::class.java))
		shortcutHandler.loadShortcuts()
	}
}
