package kalucki.przemyslaw.wakeonlan.Database

import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE

@Dao
interface DeviceDAO
{
	@Query("SELECT * FROM devices")
	fun getAll(): List<Device>

	@Query("SELECT * FROM devices WHERE id = :id")
	fun getById(id: Long): Device?

	@Insert(onConflict = REPLACE)
	fun insert(device: Device)

	@Update(onConflict = REPLACE)
	fun update(device: Device)

	@Delete
	fun delete(device: Device)

	@Query("DELETE FROM devices")
	fun deleteAll()

}