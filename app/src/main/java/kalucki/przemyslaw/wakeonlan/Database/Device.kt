package kalucki.przemyslaw.wakeonlan.Database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "devices")
data class Device constructor(@PrimaryKey(autoGenerate = true) var id: Long = 0,
                              @ColumnInfo(name = "name") var name: String = "",
                              var ip: String = "",
                              var mac: String = "",
                              var port: Int = 0)
{
	override fun toString(): String
	{
		return "Device: $id $name $ip $mac $port"
	}
}