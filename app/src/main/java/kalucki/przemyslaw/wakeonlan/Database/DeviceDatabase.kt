package kalucki.przemyslaw.wakeonlan.Database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = [(Device::class)], version = 1, exportSchema = false)
abstract class DeviceDatabase : RoomDatabase()
{
	abstract fun deviceDao(): DeviceDAO

	companion object
	{
		private const val DB_NAME = "kalucki_przemyslaw_devices_db"
		private var dbInstance: DeviceDatabase? = null

		fun getDatabase(context: Context): DeviceDatabase
		{
			if (dbInstance == null)
			{
				dbInstance = Room.databaseBuilder<DeviceDatabase>(context, DeviceDatabase::class.java, DB_NAME).build()
			}

			return dbInstance!!
		}
	}
}